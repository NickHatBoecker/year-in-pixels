import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App.vue'
import router from './router'

import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

Vue.use(Vuetify)

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')

const isProd = process.env.NODE_ENV === 'production'
const serviceWorkerIsAvailable = 'serviceWorker' in navigator && isProd
if (serviceWorkerIsAvailable) {
    navigator.serviceWorker.register('/service-worker.js').then(() => {
        console.log('Service Worker for production environment registered!')
    })
}
